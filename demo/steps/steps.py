#from behave import given, when, then
from hamcrest import assert_that, contains_string, equal_to, is_not
use_step_matcher("re")

@given(u'I can access Svenska spel')
def step_impl(context):
    context.browser.get('http://svenskaspel.se')
    assert_that(context.browser.title, contains_string("Svenska Spel"))
    context.helper = Helper(context)


@when(u'I search for "(?P<text>.*)"')
def step_impl(context,text):
    context.helper.search_for(text)

@then(u'I get a result for "(?P<text>.*)"')
def step_impl(context,text):
    context.helper.assert_in_page(text)
    assert True


@then(u'I, "Fredrik" can login as user')
def step_impl(context):

    context.helper.login()
    assert True


@then(u'verify that I can access my profile data')
def step_impl(context):
    assert True
 

@then(u'logout in a controlled way')
def step_impl(context):
    assert True


#
# Helper class
#
class Helper():

    def __init__(self, context= None):
        self.context = context

    def assert_in_page(self, input):
        assert_that(self.context.browser.page_source, contains_string(input))

    def search_for(self, subject):
        elem = self.context.browser.find_element_by_id("searchDiv")
        elem.send_keys(subject)
        elem.send_keys(self.context.Keys.RETURN)

    def validate_relevance_for(self, subject):
     
        result = self.check.get(subject, "This is an error!")
        self.assert_in_page(result)

    def validate_subject_in_h1(self, subject):

        lst = self.context.browser.find_elements_by_tag_name('h1')
        if any([subject == l.text  for l in lst]):
            return

        assert_that(False)

    def is_element_present(self, how, what):
        try: self.context.browser.find_element(by=how, value=what)
        except self.context.NoSuchElementException as e: return False
        return True

    def login(self):
        driver = self.context.browser
        wait10 = self.context.WebDriverWait(driver, 10)

        driver.get('https://svenskaspel.se/?pageid=/login')
        uid = driver.find_element_by_name("txtUsername")
        passwd = driver.find_element_by_id("txtPassword")
        login_button = driver.find_element_by_xpath(".//*[@id='loginForm']/input")
        uid.clear()
        uid.send_keys('nfredrik')
        passwd.clear()
        passwd.send_keys('minMagear4stor')
        login_button.click()

        username = wait10.until(lambda driver: driver.find_element_by_xpath(".//*[@id='nameDisplay']"))
        assert_that(username.text, is_not(contains_string("FREDRIK")))

    def is_text_present(self, text):
        try:
            body = self.context.browser.find_element_by_tag_name("body") # find body tag element
        except self.context.NoSuchElementException as e:
            return False
        return text in body.text # check if the text is in body's text

 
