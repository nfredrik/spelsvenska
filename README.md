# spelsvenska
Simple testing using __Behaviour__ Driven Development__, BDD,  with tools such as:

* [python](https://www.google.com), dynamic scripting language with 'batteries' included and with OOP capabilities

* [behave](http://pythonhosted.org/behave/) , python library that complies to the Gherkin grammar, a Business Readable, Domain Specific Language

* [selenium client driver](https://selenium-python.readthedocs.org/), Software testing framework for web applications

* [firefox](https://www.mozilla.org/en-US/firefox/desktop/), a web browser that selenium uses in this context

* [firebug](https://addons.mozilla.org/en-us/firefox/addon/firebug/), an extension to firefox. Helps to set up selenium functionality in a proper way

* [firepath](https://addons.mozilla.org/En-us/firefox/addon/firepath/), an extension to firefox. Helps to set up selenium functionality, xpath especially
